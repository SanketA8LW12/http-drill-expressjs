const express = require('express')
const app = express();
const { port } = require('./config')
const http = require('http');
const uuid = require('uuid');
const fs = require('fs');
const path = require('path');
const logFilePath = path.join(__dirname, './urlLogFile.log');
const requestId = require('express-request-id');

app.use(requestId());

function urlLog(request, response, next) {
    const requestedURL = request.url;
    let data = `Request Id : ${request.id}, Method Type : ${request.method}, request url : ${requestedURL}`
    fs.appendFile(logFilePath, `${data}  ${new Date()} \n \n`, (error, data) => {
        if (error) {
            next({
                message : error,
                status : 400
            })
        }
    });
    next();
}

app.use(urlLog);


app.get('/html', (request, response) => {
    response.send(`<!DOCTYPE html>
    <html>
      <head>
      </head>
      <body>
          <h1>Any fool can write code that a computer can understand. Good programmers write code that humans can understand.</h1>
          <p> - Martin Fowler</p>
      </body>
    </html>`)
})

app.get('/json', (request, response) => {
    response.send({
        "slideshow": {
            "author": "Yours Truly",
            "date": "date of publication",
            "slides": [
                {
                    "title": "Wake up to WonderWidgets!",
                    "type": "all"
                },
                {
                    "items": [
                        "Why <em>WonderWidgets</em> are great",
                        "Who <em>buys</em> WonderWidgets"
                    ],
                    "title": "Overview",
                    "type": "all"
                }
            ],
            "title": "Sample Slide Show"
        }
    })
})

app.get('/uuid', (request, response) => {
    response.send({
        uuid: uuid.v4()
    })
})

app.get('/status/:status', (request, response, next) => {
    const status = Number(request.params.status);
    let statusCode = http.STATUS_CODES[status];

    if (statusCode === undefined || status === undefined) {
        //response.statusCode = 404;
        next({
            message: "Bad status request",
            status: 404
        })
    }
    else {
        response.statusCode = Number(status);
        response.send({
            "status code": status
        });
    }

})

app.get('/delay/:time', (request, response, next) => {
    time = Number(request.params.time);

    if (time < 0 || isNaN(time)) {
        next({
            message: "Time cannot be negative or in words",
            status: 400
        })
    }
    else {
        setTimeout(() => {
            response.send({
                200: "OK"
            })
        }, time * 1000);
    }
})

app.get('/', (request, response) => {

    response.send('Home Page')
})

app.get('/log', (request, response) => {
    response.sendFile(logFilePath);
})

app.use((request, response) => {
    response.status(404).send({
        error: "Invalid URL"
    })
})
app.use((error, request, response, next) => {
    response.send({
        message: error.message,
        status : error.status
    })
})


app.listen(port, () => {
    console.log(`Example app listening on port ${port}`)
})